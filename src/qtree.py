# coding: utf8

import math
# import pdb


class Point(object):
    def __init__(self, x, y, value=None):
        self.x = x
        self.y = y
        self.value = value

    def __repr__(self):
        return "Point at ({},{}), value = '{}'".format(self.x,
                                                       self.y,
                                                       self.value)


class Cell(object):
    # NorthWest # 0:0
    # NorthEast # 0:1
    # SouthWest # 1:0
    # SouthEast # 1:1

    # pdb.set_trace()

    def __init__(self, min_x, max_x, min_y, max_y, max_points):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.max_points = max_points

        # square area
        self.size = (self.max_x - self.min_x) ** 2

        self.center = None
        self.points = []

    def can_subdivide(self):
        return self.size > 1

    def is_subdivided(self):
        return self.center != None

    def subdivide(self, point):
        if not self.can_subdivide():
            raise Exception("no more space")

        self.center = Point(x=((self.max_x + self.min_x) / 2.0),
                            y=((self.max_y + self.min_y) / 2.0))

        self.NorthWest = Cell(min_x=self.min_x,
                              max_x=math.floor(self.center.x),
                              min_y=math.ceil(self.center.y),
                              max_y=self.max_y,
                              max_points=self.max_points)
        self.NorthEast = Cell(min_x=math.ceil(self.center.x),
                              max_x=self.max_x,
                              min_y=math.ceil(self.center.y),
                              max_y=self.max_y,
                              max_points=self.max_points)
        self.SouthWest = Cell(min_x=self.min_x,
                              max_x=math.floor(self.center.x),
                              min_y=self.min_y,
                              max_y=math.floor(self.center.y),
                              max_points=self.max_points)
        self.SouthEast = Cell(min_x=math.ceil(self.center.x),
                              max_x=self.max_x,
                              min_y=self.min_y,
                              max_y=math.floor(self.center.y),
                              max_points=self.max_points)

        for p in self.points:
            self.__subcell_insert(p)
        self.points = []

        return self.__subcell_insert(point)

    def in_range(self, point):
        if not self.min_x <= point.x <= self.max_x:
            return False

        if not self.min_y <= point.y <= self.max_y:
            return False

        return True

    def __subcell_insert(self, point):
        if self.NorthWest.insert(point):
            return True

        if self.NorthEast.insert(point):
            return True

        if self.SouthWest.insert(point):
            return True

        if self.SouthEast.insert(point):
            return True

        # cannot be
        return False

    def insert(self, point):
        # TODO: if point exists, update value

        if not self.in_range(point):
            # print "point is out of range: {}".format(point)
            return False

        if self.is_subdivided():
            return self.__subcell_insert(point)

        # try to insert to self
        if len(self.points) >= self.max_points:
            return self.subdivide(point)

        self.points.append(point)

        return True

    def find_closest(self, point):
        raise Exception("Not implemented")

    def find_in_bouding_box(self, x_box, y_box):
        raise Exception("Not implemented")

    def read(self, point):
        if not self.in_range(point):
            raise Exception("wrong read")

        if self.is_subdivided():
            if self.NorthWest.in_range(point):
                return self.NorthWest.read(point)

            if self.NorthEast.in_range(point):
                return self.NorthEast.read(point)

            if self.SouthWest.in_range(point):
                return self.SouthWest.read(point)

            if self.SouthEast.in_range(point):
                return self.SouthEast.read(point)

            # never happens
            raise Exception("could not drop point into any of subcells")

        found = [p for p in self.points if p.x == point.x and p.y == point.y]

        if len(found) == 0:
            raise Exception("no such point: {}".format(point))

        return found[0]


class QuadTree(Cell):
    def __init__(self, pow2, max_points):
        if pow2 % 4 != 0:
            raise Exception("must be power of 4")
        size = 2 ** pow2
        self.bottom = -size
        self.top = size - 1

        super(QuadTree, self).__init__(min_x=self.bottom,
                                       max_x=self.top,
                                       min_y=self.bottom,
                                       max_y=self.top,
                                       max_points=max_points)

if __name__ == "__main__":
    qt = QuadTree(16, 1)
    qt.insert(Point(x=1000, y=-5000, value="Foo"))
    qt.insert(Point(x=1, y=0, value="Bar"))
    print qt.read(Point(x=1000, y=-5000))
    print qt.read(Point(x=1, y=0))
