TEST_RUNNER := python test.py
QTREE_DEBUG := python src/qtree.py

all:
	$(TEST_RUNNER)

standalone:
	$(QTREE_DEBUG)

clean:
	find -name '*.pyc' -o -name '*~' -delete

.PHONY: all clean standalone
