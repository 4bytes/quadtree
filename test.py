import unittest
from src . qtree import *


class SimpleTests(unittest.TestCase):

    def setUp(self):
        self.qt = QuadTree(16, 1)

    # positive tests

    def test_insert_point(self):
        self.assertTrue(self.qt.insert(Point(x=0, y=0, value='Foo')))
        self.assertTrue(self.qt.insert(Point(x=1, y=0, value='Bar')))
        self.assertEqual(self.qt.read(Point(x=1, y=0)),
                         "Point at (1,0), value = 'Bar'")

    # def test_read_point(self):
    #     self.assertEqual(self.qt.read(Point(x=1, y=0)),
    #                      "Point at (1,0), value = 'Bar'")

if __name__ == '__main__':
    unittest.main()
